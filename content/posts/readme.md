---
title: "Readme"
date: 2021-03-25T15:39:47-07:00
draft: false
---

# Hugo Project

Rendered Site: https://venomouswyvern241.gitlab.io/home/

## Documenation

QuickStart: https://gohugo.io/getting-started/quick-start/

Publish to GitLab: https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/
* for the image use: `image: registry.gitlab.com/pages/hugo/hugo_extended:<#.#.#|latest>` [hugo issue #31](https://gitlab.com/pages/hugo/-/issues/31)
* website is available at `https://<YourUsername>.gitlab.io/<your-hugo-site>/`

## Sources

Download: https://github.com/gohugoio/hugo/releases
* specifically [Windows 64 bit v0.82.0 extended](https://github.com/gohugoio/hugo/releases/download/v0.82.0/hugo_extended_0.82.0_Windows-64bit.zip)

Themes: https://themes.gohugo.io/
* [m10c](https://themes.gohugo.io/hugo-theme-m10c/)
